/*
 * MadgwickFilter.h
 *
 *  Created on: 6 Feb 2017
 *      Author: walkingbeard
 */

#ifndef MADGWICKFILTER_H_
#define MADGWICKFILTER_H_

#include "rodos.h"
#include "SensorVal.h"

extern TTime tmInterval;

#define FILTER_GAIN 5

class MadgwickFilter {
private:
    float beta;
    float interval;
    float q0;
    float q1;
    float q2;
    float q3;

public:
    MadgwickFilter();
    void filterUpdate(float gx, float gy, float gz, float ax, float ay,
                      float az, float mx, float my, float mz);
    SensorVal getEulerAnglesRad();
};

#endif /* MADGWICKFILTER_H_ */
