/*
 * HandlerAttitudeMotor.h
 *
 *  Created on: 5 Jan 2017
 *      Author: walkingbeard
 */

#ifndef HANDLERATTITUDEMOTOR_H_
#define HANDLERATTITUDEMOTOR_H_

#include "subscriber.h"
#include "thread.h"
#include "topic.h"
#include "tmTcTopics.h"
#include "AttitudeMotor.h"

#define MAX_MOTOR_SPEED 1000

class HandlerAttitudeMotor: public RODOS::Thread,
        public RODOS::SubscriberReceiver<TC_Motor> {
private:
    float desiredSpeed;

public:
    HandlerAttitudeMotor();
    void init();
    void run();
    void put(TC_Motor &data);
};

#endif /* HANDLERATTITUDEMOTOR_H_ */
