/*
 * attitudeMotorEncoder.h
 *
 *  Created on: 18 Jan 2017
 *      Author: walkingbeard
 */

#ifndef ATTITUDEMOTORENCODER_H_
#define ATTITUDEMOTORENCODER_H_

#define EDGE_PER_CHAN_PER_REV 16

extern bool encWorking;

// Returns the frequency of the attitude motor, with -ve meaning -ve rotation
extern int32_t readEncoder();
extern void    initEncoder();

#endif /* ATTITUDEMOTORENCODER_H_ */
