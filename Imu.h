/*
 * Imu.h
 *
 *  Created on: 3 Jan 2017
 *      Author: walkingbeard
 */

#ifndef IMU_H_
#define IMU_H_

#ifndef M_PI
    #define M_PI (3.14159265358979323846)
#endif

#include "rodos.h"
#include "hal.h"
#include "SensorVal.h"

// Device select pins
#define GYRO_SELECT_PIN   GPIO_018 // Gyroscope
#define MAGACC_SELECT_PIN GPIO_032 // Magnetometer/accelerometer

// IMU enable pin
#define ENABLE_IMU_PIN GPIO_055

// I2C bus number
#define I2C_BUS_NUM I2C_IDX2

// Device I2C addresses
#define GYRO_ADDR   0x6B    // Gyroscope
#define MAGACC_ADDR 0x1D    // Magnetometer/accelerometer

// IMU identification register address (same for all sensors)
#define WHO_AM_I 0x0F

// IMU identification register values
#define WHO_AM_I_XM_VAL 0b01001001
#define WHO_AM_I_G_VAL 0b01001001

/* IMU output register addresses
 * Only the lowest address in each trio of 2 bytes is
 * required for multibyte reads. */
#define OUT_X_L_M   0x08    // Magnetometer
#define OUT_X_L_A   0x28    // Accelerometer
#define OUT_X_L_G   0x28    // Gyroscope

/* IMU control registers
 * Only those used for SONS floatsat*/
#define CTRL_REG1_XM 0x20
#define CTRL_REG2_XM 0x21
#define CTRL_REG5_XM 0x24
#define CTRL_REG6_XM 0x25
#define CTRL_REG7_XM 0x26
#define CTRL_REG1_G  0x20
#define CTRL_REG4_G  0x23

/* Definition of mission-specific control parameters for
 * each sensor on the IMU */

// Acceleration ODR=200Hz, BDU enabled, acceleration axis enabled
#define CTRL_REG1_XM_VAL 0b01111111
// Accn anti-alias filter BW=50Hz, accn full scale = +-2g, normal mode
#define CTRL_REG2_XM_VAL 0b11000000
// Temp sensor disable, Magnetic Resolution Low, Magnetic Data Rate = 100 Hz
#define CTRL_REG5_XM_VAL 0b00010100
// Magnetic full scale = +-2 gauss
#define CTRL_REG6_XM_VAL 0b00000000
// Normal mode, internal filter bypassed, continuous conversion mode
#define CTRL_REG7_XM_VAL 0b10000000
// ODR=190Hz (2), Cutoff=12.5Hz (2), Normal Mode (1), Gyro Axis Enabled (3)
#define CTRL_REG1_G_VAL  0b01001111
// BDU Enabled, Data LSb @ Lower Address, Full scale = 2000 dps,
// Self-test Disabled
#define CTRL_REG4_G_VAL  0b10100000

// Sensor sensitivities
#define GYRO_SENSITIVITY 0.07     // 70mdps/digit
#define MAG_SENSITIVITY  0.08     // 0.08mg/LSB
#define ACC_SENSITIVITY  0.000061 // 0.061mgauss/LSB

#define ACC_OFFSET_X -998.46275
#define ACC_OFFSET_Y -793.53025
#define ACC_OFFSET_Z -442.2315

#define MAG_MIN_X -3441.75
#define MAG_MIN_Y -665.75
#define MAG_MIN_Z 4832.5

#define MAG_MAX_X -1276.5
#define MAG_MAX_Y 1027.5
#define MAG_MAX_Z 5574.25


class Imu {
private:
    HAL_GPIO chipSelectGyro;
    HAL_GPIO chipSelectMagAcc;
    HAL_GPIO enableImu;
    HAL_I2C  imuI2C;
    SensorVal gyroOffset;
    SensorVal accOffset;
    SensorVal magMax;
    SensorVal magMin;

    // Following functions returning 8-bit integers return 0 on success
    // and -1 on failure unless otherwise specified

    // Sets/gets contents of a single 8-bit register
    int8_t setRegister(uint8_t dev, uint8_t reg, uint8_t val);
    int8_t getRegister(uint8_t dev, uint8_t reg, uint8_t &val);
    
    // Reads the raw, unnormalised sensor values from a single sensor (3 words)
    int8_t readRaw(uint8_t dev, uint8_t startReg, int16_t *data);

    // Device initialisers
    int8_t initGyro();
    int8_t initMagAcc();


public:
    Imu();

    // Used for resetting the imu and I2C
    void init();
    void disable();

    // Read the sensors with appropriate offsets and scale adjustment
    int8_t readGyro(SensorVal &data, bool adjust);
    int8_t readMag(SensorVal &data, bool adjust);
    int8_t readAcc(SensorVal &data, bool adjust);

    // Set sensor offsets
    void setOffsetGyro(const SensorVal &offset);
    void setOffsetAcc(const SensorVal &offset);
    void setOffsetMag(const SensorVal &max, const SensorVal &min);

    void printOffsets();

    // Checks the identities of both sensors, returns 0 on success
    int8_t checkIdGyro();
    int8_t checkIdMagAcc();

    float getStableHeading(SensorVal acc, SensorVal mag);
};

#endif /* IMU_H_ */
