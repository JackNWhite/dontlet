/*
 * LedManager.h
 *
 *  Created on: 2 Jan 2017
 *      Author: walkingbeard
 */

#ifndef LEDMANAGER_H_
#define LEDMANAGER_H_

#include "rodos.h"

#define LED_GREEN GPIO_060
#define LED_ORANGE GPIO_061
#define LED_RED GPIO_062
#define LED_BLUE GPIO_063

enum ledColour {
    GREEN, BLUE, RED, ORANGE
};

class LedManager {
private:
    HAL_GPIO green;
    HAL_GPIO blue;
    HAL_GPIO red;
    HAL_GPIO orange;
public:
    void switchLED(ledColour col);
    void switchOnLED(ledColour col);
    void switchOffLED(ledColour col);

    LedManager();
};

#endif /* LEDMANAGER_H_ */
