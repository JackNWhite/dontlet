/*
 * tmTcTopics.h
 *
 *  Created on: 22 Dec 2016
 *      Author: walkingbeard
 *
 * Contains structures for passing of telecommands and telemetry between the
 * satellite and the groundstation. These are defined by the cross-team
 * ground station devs.
 */

#ifndef TMTCTOPICS_H_
#define TMTCTOPICS_H_

#include <string>
#include "rodos.h"

//==================== Telemetry ====================//

struct TM_InertialMeasurements {
    float raw_pitch; // rad
    float raw_roll; // rad
    float raw_yaw; // rad
    float raw_pitch_velocity; // rad/sec
    float raw_roll_velocity; // rad/sec
    float raw_yaw_velocity; // rad/sec
    float pitch; // rad
    float roll; // rad
    float yaw; // rad
    float pitch_velocity; // rad/sec
    float roll_velocity; // rad/sec
    float yaw_velocity; // rad/sec
};

struct TM_LightSensor {
    float value;
    float ignore_1 = 0;
    float ignore_2 = 0;
    float ignore_3 = 0;
    float ignore_4 = 0;
};

struct TM_BatteryPower {
    float voltage;
    float current;
};

struct TM_SolarPower {
    float voltage;
    float current;
};

struct TM_ThermalKnife {
    uint8_t value;
};

struct TM_Motor {
    float value;
};


struct TM_STMImage {
    uint8_t format;
    uint32_t bytes_count;
    uint8_t bytes[];
};

struct TM_PIDValues {
    uint8_t controller;
    float p;
    float i;
    float d;
};

struct TM_Connection {
    uint8_t connection;
};

//==================== Telecommand ====================//

struct TC_ThermalKnife {
    float value;
};

struct TC_PIDValues {
    uint8_t controller;
    float p;
    float i;
    float d;
};

struct TC_Motor {
    float value;
};

struct TC_JoystickValues{
    float value;
};

struct TC_DockingGo{
    uint8_t go;
};

struct TC_AngleRotation {
    float value;
};

struct TC_VelocityRotation{
    float value;
};


struct TC_STMImage {
    uint8_t snap;
};



enum SensorType {
    ACCELEROMETER = 1, MAGNETOMETER =2 , GYROSCOPE = 3
};

enum Axis {
    X = 1, Y =2 , Z =3 , FINAL = 0
};

struct TC_ImuCalib {
    SensorType sensor;
    Axis       axis;
};

struct TC_TelemetryInterval {
    TTime ns;
};

// RODOS topics

extern RODOS::Topic<TM_InertialMeasurements> topicTMInertialMeasurements;
extern RODOS::Topic<TM_LightSensor> topicTMLightSensor;
extern RODOS::Topic<TM_BatteryPower> topicTMBatteryPower;
extern RODOS::Topic<TM_SolarPower> topicTMSolarPower;
extern RODOS::Topic<TM_STMImage> topicTMSTMImage;
extern RODOS::Topic<TM_ThermalKnife> topicTMThermalKnife;
extern RODOS::Topic<TM_PIDValues> topicTMPIDValues;
extern RODOS::Topic<TM_Motor> topicTMMotor;
extern RODOS::Topic<TC_ThermalKnife> topicTCThermalKnife;
extern RODOS::Topic<TC_PIDValues> topicTCPIDValues;
extern RODOS::Topic<TC_Motor> topicTCMotor;
extern RODOS::Topic<TC_JoystickValues> topicTCJoystickValues;
extern RODOS::Topic<TC_DockingGo> topicTCDockingGo;
extern RODOS::Topic<TC_AngleRotation> topicTCAngleRotation;
extern RODOS::Topic<TC_VelocityRotation> topicTCVelocityRotation;

extern RODOS::Topic<char *> topicErrorReports;
extern RODOS::Topic<TC_ImuCalib> topicTCImuCalibration;
extern RODOS::Topic<TC_TelemetryInterval> topicTMInterval;

extern RODOS::Topic<char *> topicReports;

#endif /* TMTCTOPICS_H_ */
