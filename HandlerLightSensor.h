/*
 * HandlerLightSensor.h
 *
 *  Created on: 1 Feb 2017
 *      Author: walkingbeard
 */

#ifndef HANDLERLIGHTSENSOR_H_
#define HANDLERLIGHTSENSOR_H_

#include <thread.h>

class HandlerLightSensor: public RODOS::Thread {
public:
    HandlerLightSensor();
    void init();
    void run();
};

#endif /* HANDLERLIGHTSENSOR_H_ */
