/*
 * attitudeEncoderV4.cpp
 *
 *  Created on: 5 Feb 2017
 *      Author: walkingbeard
 */

#include "attitudeEncoderV4.h"

void initEncoder()
{
    // STM intialisation structures
    GPIO_InitTypeDef        initialisersPA5;
    TIM_TimeBaseInitTypeDef initialisersTIM2;

    initialisersPA5.GPIO_Pin = GPIO_Pin_5;
    initialisersPA5.GPIO_Mode = GPIO_Mode_AF;

    initialisersTIM2.TIM_Prescaler = 0;
    initialisersTIM2.TIM_CounterMode = TIM_CounterMode_Up;
    initialisersTIM2.TIM_Period = 100000;
    initialisersTIM2.TIM_ClockDivision = TIM_CKD_DIV1;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_TIM2);
    GPIO_Init(GPIOA, &initialisersPA5);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    TIM_TimeBaseInit(TIM2, &initialisersTIM2);
    TIM_ETRClockMode2Config(TIM2, TIM_ExtTRGPSC_OFF,
                            TIM_ExtTRGPolarity_NonInverted, 0x00);
    TIM_Cmd(TIM2, ENABLE);
}   


float readEncoder(uint8_t msTime)
{
    float microSecs((float) msTime / 1000);
    float retVal((float) TIM_GetCounter(TIM2));

    TIM_SetCounter(TIM2, 0);
    retVal = retVal / (microSecs * EDGES_PER_ROTATION);
    return retVal;
}
