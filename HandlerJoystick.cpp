/*
 * HandlerJoystick.cpp
 *
 *  Created on: 5 Feb 2017
 *      Author: walkingbeard
 */

#include "HandlerJoystick.h"
#include "components.h"

HandlerJoystick::HandlerJoystick() :
        Thread(), SubscriberReceiver(topicTCJoystickValues), curVal(0)
{
}

void HandlerJoystick::init()
{
}

void HandlerJoystick::run()
{

}

void HandlerJoystick::put(TC_JoystickValues &newVal)
{
    int16_t tempVal;

    if (newVal.value < -JOYSTICK_MAX_VAL || newVal.value > JOYSTICK_MAX_VAL)
        return;

    tempVal = (int16_t) ((newVal.value / (float) JOYSTICK_MAX_VAL) * (float) MOTOR_MAX_VAL);

    attitudeMotor.setSpeed(tempVal);
}
