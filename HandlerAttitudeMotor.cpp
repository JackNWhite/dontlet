/*
 * HandlerAttitudeMotor.cpp
 *
 *  Created on: 5 Jan 2017
 *      Author: walkingbeard
 */

#include "HandlerAttitudeMotor.h"
#include "components.h"

HandlerAttitudeMotor::HandlerAttitudeMotor() :
        Thread("Attitude motor handler"), SubscriberReceiver<TC_Motor>(
                topicTCMotor, "Attitude motor handler"), desiredSpeed(0)
{
    // Empty
}


void HandlerAttitudeMotor::put(TC_Motor &data)
{
    TM_Motor spd;

    // TODO: replace this with setting desired speed for controller
    char *msg = "Motor commanded to attain speed";
    topicReports.publish(msg, false);

    int16_t speed(data.value);
    desiredSpeed = data.value;

    if (speed > MAX_MOTOR_SPEED) {
        spd.value = MAX_MOTOR_SPEED;
        attitudeMotor.setSpeed(MAX_MOTOR_SPEED);
        //topicTMMotor.publish(spd, true);
    } else if (speed < -MAX_MOTOR_SPEED) {
        spd.value = -MAX_MOTOR_SPEED;
        attitudeMotor.setSpeed(-MAX_MOTOR_SPEED);
        //topicTMMotor.publish(spd, true);
    } else {
        spd.value = data.value;
        attitudeMotor.setSpeed(speed);
        //topicTMMotor.publish(spd, true);
    }
}


void HandlerAttitudeMotor::init()
{

}

void HandlerAttitudeMotor::run()
{
    TM_Motor curDesiredSpeed;
    while (1) {
        curDesiredSpeed.value = desiredSpeed;
//        topicTMMotor.publish(curDesiredSpeed, true);
        TM_Connection dummyVar;
        suspendCallerUntil(NOW() + tmInterval);
    }
}

