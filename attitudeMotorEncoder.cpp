/*
 * AttitudeMotorEncoder.cpp
 *
 *  Created on: 11 Jan 2017
 *      Author: walkingbeard
 * Description: Uses GPIOA3 and 5 to run the attitude motor encoder by setting
 *              GPIOA3 as the interrupt generator and attaching it to STM32F4
 *              timer 2 (channel 4) and checking the other GPIOA5 (encoder chan)
 *              to ascertain motor direction
 */

#include "stm32f4xx_rcc.h"
#include "stm32f4xx_tim.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_misc.h"
#include "attitudeMotorEncoder.h"

bool encWorking = false;

extern "C" {
    // Modified from STM32F4 peripheral example library
    // __IO appears to be alias for volatile storage qualifier - stops the
    // compiler from optimising code involving these variables because of the
    // side-effect of the interrupt
    __IO uint16_t encValCapture1 = 0;
    __IO uint16_t encValCapture2 = 0;
    __IO uint16_t encCaptureNumber = 0;
    __IO uint32_t encCapture = 0;
    __IO uint32_t encTim2Freq = 0;
    __IO int8_t   encDirection = 0;


    void TIM2_CC_IRQHandler(void)
    {
        encWorking=true;
        if(TIM_GetITStatus(TIM2, TIM_IT_CC4) == SET) {
            /* Clear TIM2 capture/compare interrupt pending bit */
            TIM_ClearITPendingBit(TIM2, TIM_IT_CC4);

            if(encCaptureNumber == 0) {
                /* Get the Input Capture value */
                encValCapture1 = TIM_GetCapture4(TIM2);
                encCaptureNumber = 1;
            } else if(encCaptureNumber == 1) {
                /* Get the Input Capture value */
                encValCapture2 = TIM_GetCapture4(TIM2); 

                /* Capture computation */
                if (encValCapture2 > encValCapture1) {
                    encCapture = (encValCapture2 - encValCapture1);
                } else if (encValCapture2 < encValCapture1) {
                    encCapture = ((0xFFFF - encValCapture1) + encValCapture2); 
                } else {
                    encCapture = 0;
                }

                /* Frequency computation */ 
                encTim2Freq = (uint32_t) SystemCoreClock / encCapture;
                encCaptureNumber = 0;
            }
            encDirection = (GPIO_ReadInputData(GPIOA) & 0x32) ? -1 : 1;
        }
    }
}


int32_t readEncoder()
{
    return encDirection * encTim2Freq / EDGE_PER_CHAN_PER_REV;
}


void initEncoder()
{
    // Configuration for encoder channel GPIOs, vector table entry and timer
    GPIO_InitTypeDef  chanAConfig; // Used to trigger timer
    GPIO_InitTypeDef  chanBConfig;
    NVIC_InitTypeDef  itrControllerConfig;
    TIM_ICInitTypeDef timerICConfig;

    // Enable clocks for (i.e. switch on) components
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE); // For both channels

    // Pin configurations
    chanAConfig.GPIO_Pin = GPIO_Pin_5;
    chanAConfig.GPIO_Mode = GPIO_Mode_AF;   // Enbl connex to TIM2
    chanAConfig.GPIO_Speed = GPIO_Speed_100MHz;
    chanAConfig.GPIO_OType = GPIO_OType_PP;
    chanAConfig.GPIO_PuPd = GPIO_PuPd_NOPULL;

    chanBConfig.GPIO_Pin = GPIO_Pin_3;
    chanBConfig.GPIO_Mode = GPIO_Mode_IN;
    chanBConfig.GPIO_Speed = GPIO_Speed_100MHz;
    chanBConfig.GPIO_OType = GPIO_OType_PP;
    chanBConfig.GPIO_PuPd = GPIO_PuPd_NOPULL;

    // Execute pin configs
    GPIO_Init(GPIOA, &chanAConfig);
    GPIO_Init(GPIOA, &chanBConfig);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_TIM2);

    // Configure entry for interrupt in interrupt vector table
    itrControllerConfig.NVIC_IRQChannel = TIM2_IRQn;
    itrControllerConfig.NVIC_IRQChannelPreemptionPriority = 0;
    itrControllerConfig.NVIC_IRQChannelSubPriority = 1;
    itrControllerConfig.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&itrControllerConfig);

    // Configure timer
    timerICConfig.TIM_Channel = TIM_Channel_2;
    timerICConfig.TIM_ICPolarity = TIM_ICPolarity_Falling;
    timerICConfig.TIM_ICSelection = TIM_ICSelection_DirectTI;
    timerICConfig.TIM_ICPrescaler = TIM_ICPSC_DIV1;
    timerICConfig.TIM_ICFilter = 0x0;

    //Execute timer config
    TIM_ICInit(TIM2, &timerICConfig);
    TIM_Cmd(TIM1, ENABLE);
    TIM_ITConfig(TIM2, TIM_IT_CC4, ENABLE); // Enable chan4 IRQ
}

