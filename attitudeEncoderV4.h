/*
 * attitudeEncoderV4.h
 *
 *  Created on: 5 Feb 2017
 *      Author: walkingbeard
 */

#ifndef ATTITUDEENCODERV4_H_
#define ATTITUDEENCODERV4_H_

#define EDGES_PER_ROTATION 16.0

#include "stm32f4xx_tim.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_misc.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_syscfg.h"
#include "stm32f4xx_conf.h"

void initEncoder();
float readEncoder(uint8_t msTime);


#endif /* ATTITUDEENCODERV4_H_ */
