/*
 * hBridge.cpp
 *
 *  Created on: 3 Jan 2017
 *      Author: walkingbeard
 */

#include "hBridge.h"

void enableHBridges()
{
    static bool hBridgeEnabled(false);
    static RODOS::HAL_GPIO hBridgeEnablePin(PIN_HBRIDGE_ENABLE);

    if (!hBridgeEnabled) {
        hBridgeEnablePin.init(true, 1, 1); // Is output, 1 pin, init high
        hBridgeEnabled = true;
    }
}
