/*
 * DockingMagnet.cpp
 *
 *  Created on: 5 Feb 2017
 *      Author: walkingbeard
 */

#include "DockingMagnet.h"

DockingMagnet::DockingMagnet() : hBridgeInA(HBRIDGE_C_INA_GPIO),
                                 hBridgeInB(HBRIDGE_C_INB_GPIO),
                                 magnetPWM(HBRIDGE_C_PWM_INDEX)
{
    magnetPWM.init(1000,100);
    magnetPWM.write(100);
    hBridgeInA.init(true, 1, 0);
    hBridgeInB.init(true, 1, 0);
    enableHBridges();
}

void DockingMagnet::switchOn()
{
    hBridgeInA.setPins(1);
    hBridgeInB.setPins(1);
}

void DockingMagnet::switchOff()
{
    hBridgeInA.setPins(0);
    hBridgeInB.setPins(0);
}
