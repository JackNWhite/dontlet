/*
 * ThermalKnife.cpp
 *
 *  Created on: 3 Jan 2017
 *      Author: walkingbeard
 */

#include "ThermalKnife.h"
#include "hBridge.h"
#include "tmTcTopics.h"
#include "components.h"
#include <string>

ThermalKnife::ThermalKnife() :
        hBridgeInA(HBRIDGE_D_INA_GPIO), modulator(PWM_MODULATOR_INDEX)
{
    modulator.init(TH_PWM_FREQ, TH_PWM_INC);
    hBridgeInA.init(true, 1, 0); // Start knife off
    modulator.write(1); // Always need full power cycle
    enableHBridges();
}

void ThermalKnife::toggle()
{
    char *msg = "Toggling thermal knife";
    topicErrorReports.publish(msg, false);
    hBridgeInA.setPins(~hBridgeInA.readPins());
}

void ThermalKnife::switchOn()
{
    char *msg = "Switching on thermal knife";
    topicErrorReports.publish(msg, false);
    hBridgeInA.setPins(1);
}

void ThermalKnife::switchOff()
{
    char *msg = "Switching off thermal knife";
    topicErrorReports.publish(msg, false);
    hBridgeInA.setPins(0);
}

