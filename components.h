/*
 * components.h
 *
 *  Created on: 5 Jan 2017
 *      Author: walkingbeard
 */

#ifndef COMPONENTS_H_
#define COMPONENTS_H_

#include "rodos.h"
#include "hBridge.h"
#include "Imu.h"
#include "LedManager.h"
#include "ThermalKnife.h"
//#include "LightSensor.h"
#include "DockingMotor.h"
#include "AttitudeMotor.h"
#include "DockingMagnet.h"
//#include "attitudeMotorEncoder.h"
#include "attitudeEncoderV4.h"
#include "lightSensor_Working.h"
#include "MadgwickFilter.h"

#define DEFAULT_GLOBAL_TM_INTERVAL 1000 // ms

// Global telemetry interval
extern TTime         tmInterval;

// Real components
extern LedManager     leds;
extern AttitudeMotor  attitudeMotor;
extern Imu            imu;
extern ThermalKnife   knife;
//extern LightSensor   lightSensor;
extern LIGHT          lightSensor;
extern DockingMotor   dockingMotor;
extern DockingMagnet  dockingMagnet;
extern MadgwickFilter madgwick;

#endif /* COMPONENTS_H_ */
