/*
 * HandlerImu.h
 *
 *  Created on: 6 Jan 2017
 *      Author: walkingbeard
 */

#ifndef HANDLERIMU_H_
#define HANDLERIMU_H_

#include "Imu.h"
#include "thread.h"
#include "subscriber.h"
#include "tmTcTopics.h"
#include "SensorVal.h"

// Number of samples taken per axis during calibration data acquistion
#define CALIB_SAMP_NUM 200
// Default period in milliseconds of sampling during calibration
#define CALIB_PERIOD_DEFAULT 10


/*
 * - Regime enum sets mode of operation. Normal telemetry will not be sent
 *   during calibration of IMU sensors.
 * - Calibration of the gyro is done in one take for all axes
 * - Calibration of the magnetometer is done on a per-axis basis
 * - Calibration of the accelerometer is a two-stage process. First there is a
 *   per-axis data collection and then the second stage utilises the data from
 *   all three collections to finalise the calibration.
 */
enum Regime {
    TELEMETRY = 1,
    CALIB_GYRO = 2,
    CALIB_ACC_X = 3,
    CALIB_ACC_Y = 4,
    CALIB_ACC_Z = 5,
    CALIB_ACC_FINAL = 6,
    CALIB_MAG_X = 7,
    CALIB_MAG_Y = 8,
    CALIB_MAG_Z = 9,
    CALIB_MAG_FINAL = 10
};


class HandlerImu: public Thread, public SubscriberReceiver<TC_ImuCalib> {
    Regime    regime;
    uint16_t  sampleNum;
    uint16_t  calibPeriod;
    SensorVal sample[CALIB_SAMP_NUM];
    SensorVal gyroOffset;
    SensorVal accnOffset;
    SensorVal accnCalibValsX;   // Avg vals for rot about X axis
    SensorVal accnCalibValsY;   // Avg vals for rot about Y axis
    SensorVal accnCalibValsZ;   // Avg vals for rot about Z axis
    SensorVal magMin;
    SensorVal magMax;
public:
    HandlerImu();
    
    // Currently empty but required by Thread class
    void init();
    // Runs loop of current regime
    void run();
    // Handles incoming calibration commands
    void put(TC_ImuCalib &command);
};

#endif /* HANDLERIMU_H_ */
