/*
 * SensorVal.cpp
 *
 *  Created on: 2 Jan 2017
 *      Author: walkingbeard
 *
 *  Description: A simple 3-vector for use in retrieving data from the IMU
 */

#include "SensorVal.h"

SensorVal::SensorVal() :
        x(0), y(0), z(0)
{
    // Empty
}

SensorVal::SensorVal(double _x, double _y, double _z) :
        x(_x), y(_y), z(_z)
{
    // Empty
}

SensorVal operator+(const SensorVal &left, const SensorVal &right)
{
    return SensorVal(left.x + right.x,
                     left.y + right.y,
                     left.z + right.z);
}

SensorVal operator-(const SensorVal &left, const SensorVal &right)
{
    return SensorVal(left.x - right.x,
                     left.y - right.y,
                     left.z - right.z);
}

SensorVal operator/(const SensorVal &left, const double &right)
{
    return SensorVal(left.x / right,
                     left.y / right,
                     left.z / right);
}

SensorVal operator*(const SensorVal &left, const double &right)
{
    return SensorVal(left.x * right,
                     left.y * right,
                     left.z * right);
}
