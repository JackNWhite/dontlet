/*
 * Template.cpp
 *
 * Created on: 25.06.2014
 * Author: Atheel Redah
 *
 */

#include "rodos.h"
#include <stdio.h>
#include "hal.h"
#include "math.h"
#include "LedManager.h"
#include "components.h"
#include "tmTcTopics.h"
#include "topic.h"
#include "SensorVal.h"

static Application module01("SONS Docking Satellite", 2001);

#define USB_UART UART_IDX3

class Telemetry: public Thread {
private:
    TC_ThermalKnife knifeTest;
    TTime started;
    bool used;
public:

    Telemetry(const char* name) :
            Thread(name), used(false)
    {
    }

    void init()
    {
        knifeTest.value = 20;
        started = NOW();
        used = false;
    }

    void run()
    {
        while (1) {
//            TTime justNow = NOW();
//            if (!used && justNow > started + 10 * SECONDS) {
//                used = true;
//                TC_ImuCalib calibOp;
//                PRINTF("Calibrating mag\n");
//                calibOp.axis = X;
//                calibOp.sensor = MAGNETOMETER;
//                topicTCImuCalibration.publish(calibOp);
//                suspendCallerUntil(NOW() + 15 * SECONDS);
//                calibOp.axis = Y;
//                topicTCImuCalibration.publish(calibOp);
//                suspendCallerUntil(NOW() + 15 * SECONDS);
//                calibOp.axis = Z;
//                topicTCImuCalibration.publish(calibOp);
//                suspendCallerUntil(NOW() + 15 * SECONDS);
//                calibOp.axis = FINAL;
//                topicTCImuCalibration.publish(calibOp);
//                suspendCallerUntil(NOW() + 1 * SECONDS);
//            }
            suspendCallerUntil(NOW()+ SECONDS);
        }
    }
};

Telemetry telemetry("Telemetry");

/***********************************************************************/
